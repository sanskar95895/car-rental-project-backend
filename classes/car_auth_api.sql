-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2022 at 10:54 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_auth_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `booked_car_id` int(150) NOT NULL,
  `user_email` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `booked_vehicle_model` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `booked_vehicle_num` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(150) NOT NULL,
  `no_of_days` int(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`booked_car_id`, `user_email`, `booked_vehicle_model`, `booked_vehicle_num`, `start_date`, `no_of_days`) VALUES
(2, 'n@gmail.com', 'BMW', 'DL123709', '1660033093535', 4),
(2, 'n@gmail.com', 'BMW', 'DL123709', '1660033101703', 4),
(8, 'khanak@gmail.com', 'Roadster', 'MP09423590', '1661502534000', 8);

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `vehicle_model` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_num` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rent_per_day` int(10) NOT NULL,
  `seating_capacity` int(10) NOT NULL,
  `car_id` int(10) NOT NULL,
  `availability` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`vehicle_model`, `vehicle_num`, `rent_per_day`, `seating_capacity`, `car_id`, `availability`) VALUES
('Maruti Alto 800', 'MP09222443', 700, 5, 1, b'0'),
('BMW ', 'DL123709', 1300, 6, 2, b'1'),
('Skoda', 'MP09678', 800, 9, 3, b'1'),
('Audi', 'MP0942359', 900, 9, 6, b'1'),
('Swift', 'MP095678', 900, 5, 7, b'1'),
('Roadster', 'MP09423590', 9000, 2, 8, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`) VALUES
(7, 'username', 'useremail@mail.com', '$2y$10$a049/pWEOm.jK5.sfRxK9.LQ.ZFTUWDiMdZ0oRT.cd7GZV0lvmX1S', 'Customer'),
(8, 'Sanskar Saxena', 'sanskarsaxena95895@gmail.com', '$2y$10$5GsOtLe9Aj80oAFrJOXmvuMbFZwgy0EluvDLTMRA2DNazLC27zu82', 'Manager'),
(9, 'saitamaa', 's@gmail.com', '$2y$10$Xc/w9yHjbVkRfM4npPWufOsQpglM5JeMaXn96OEQisNQh2Y6wrTbS', 'Customer'),
(10, 'Gajodhar Singh', 'gajju@gmail.com', '$2y$10$umFctW9/mSfXulpI54JPJ.t/19h33iYhb4cVS.8XUSPGtKj98874K', 'Manager'),
(11, 'shraddha', 'n@gmail.com', '$2y$10$D4xIuQLlySjNGYNAKKo9IOKhnm0hr57TFOVjNMtbA5VrsTXy795UC', 'Customer'),
(12, 'sharad', 'hghv@gmail.com', '$2y$10$sSQ8iVRs0ZfH34YELfZGzuTB2wCk/AEAlTSx8274i6SxwVLNhdyUi', 'Customer'),
(13, 'sharadkushwah', 'sharad@gmail.com', '$2y$10$dWIKEU57pylWbdkFdfbnDe/SDwQqrBQYNq04TjcAiLnBN/pVx2Q3i', 'Customer'),
(14, 'sweety', 'sweety@gmail.com', '$2y$10$shFg.3T5ejsqCoRFRAPCOO8kTZOcfUF0YgmZEGRb.vp9OnWB2e042', 'Customer'),
(15, 'saitamaa', '19btc055@ietdavv.edu.in', '$2y$10$Uq05U/YZJV3X9/cmzHygO.qjR4BJgDIMYvBOh5HHyDlG3hhgK2p/6', 'Customer'),
(16, 'sanskar1209', 'khanak@gmail.com', '$2y$10$ClH6YxAqDmd/51qUN0WRNOHLPmBNtemaS2lczjhh6y6YLEnbek0MC', 'Customer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`car_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `car_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
